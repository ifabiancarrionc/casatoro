<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Storage;
use DB;
use Session;

class AdminController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $products = Product::orderBy('updated_at','desc')->paginate(10);
        $products->withPath('api/products');
        return view('admin.product', ['products' => $products]);
    }
    
    public function products(Request $request) {
        return Product::select('*',DB::raw('price*quantity as total'))
                ->orderBy('updated_at','desc')
                ->paginate(10);
    }

    public function storeProduct(Request $request) {
        $rules = [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'image' => 'required|image',
            'quantity' => 'required|int|min:0',
            'price' => 'required|int|min:0',
        ];
        $this->validate($request, $rules);
        $path = Storage::disk('public_folder')->putFile('image',$request->file('image'));
        $data = $request->all();
        $data['image'] = $path;
        $product = Product::create($data);
        Session::flash('success', 'Producto creado correctamente');
        return redirect('/'.$product->id.'/edit');
    }
    
    public function editProduct($id, Request $request) {
        $product = Product::find($id);
        return view('admin.edit', ['product' => $product]);
    }

    public function updateProduct($id, Request $request) {
        $rules = [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'image' => 'image',
            'quantity' => 'required|int|min:0',
            'price' => 'required|int|min:0',
        ];
        $this->validate($request, $rules);
        $data = $request->all();
        if(isset($data['image'])){
            $path = Storage::disk('public_folder')->putFile('image', $request->file('image'));
            $data['image'] = $path;
        }
        $product = Product::find($id);
        $product->createHistoryDiff($data);
        $product->update($data);
        Session::flash('success', 'Producto actualizado correctamente');
        return redirect('/'.$product->id.'/edit');
    }

}
