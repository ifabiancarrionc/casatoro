<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Product extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'price', 'image', 'quantity'];

    public function productHistory() {
        return $this->hasMany('App\Models\ProductHistory')->orderBy('created_at','desc');
    }

    public function createHistoryDiff($data) {
        $description = '';
        if ($this->name != $data['name']) {
            $description .= 'Cambio de nombre '.$this->name.' a ' . $data['name'] . '. ';
        }
        if ($this->description != $data['description']) {
            $description .= 'Cambio de descripción '.$this->description.' a ' . $data['description'] . '. ';
        }
        if ($this->quantity != $data['quantity']) {
            $description .= 'Actualizacion de articulos disponibles de '.$this->quantity.' a ' . $data['quantity'] . '. ';
        }
        if ($this->price != $data['price']) {
            $description .= 'Actualizacion de price de '.$this->price.' a ' . $data['price'] . '. ';
        }
        if (isset($data['image'])) {
            $description .= 'Cambio de imagen. ';
        }
        if($description){
            $data = [
                'name' => 'Actualización',
                'description' => $description,
                'quantity' => $data['quantity'],
                'price' => $data['price'],
                'user_id' => Auth::user()->id
            ];
            $this->productHistory()->create($data);
        }
    }

    public static function boot() {
        parent::boot();

        self::created(function($product) {
            $data = [
                'name' => 'Stock Inicial',
                'description' => 'Este producto inicia con ' . $product->quantity . ' elementos',
                'price' => $product->price,
                'quantity' => $product->quantity,
                'user_id' => Auth::user()->id
            ];
            $product->productHistory()->create($data);
        });
    }

}
