<?php

use Illuminate\Database\Seeder;

class ProductHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_history')->insert([
            'id' => 1,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 10 elementos",
            'price' => "72000",
            'quantity' => 10,
            'product_id' => 1,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product_history')->insert([
            'id' => 2,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 5 elementos",
            'price' => "80000",
            'quantity' => 5,
            'product_id' => 2,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product_history')->insert([
            'id' => 3,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 12 elementos",
            'price' => "81000",
            'quantity' => 12,
            'product_id' => 3,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product_history')->insert([
            'id' => 4,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 8 elementos",
            'price' => "77000",
            'quantity' => 8,
            'product_id' => 4,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product_history')->insert([
            'id' => 5,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 7 elementos",
            'price' => "42000",
            'quantity' => 7,
            'product_id' => 5,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product_history')->insert([
            'id' => 6,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 0 elementos",
            'price' => "18000",
            'quantity' => 0,
            'product_id' => 6,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product_history')->insert([
            'id' => 7,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 13 elementos",
            'price' => "44000",
            'quantity' => 13,
            'product_id' => 7,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product_history')->insert([
            'id' => 8,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 13 elementos",
            'price' => "35000",
            'quantity' => 8,
            'product_id' => 8,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product_history')->insert([
            'id' => 9,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 0 elementos",
            'price' => "18000",
            'quantity' => 0,
            'product_id' => 9,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product_history')->insert([
            'id' => 10,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 18 elementos",
            'price' => "39000",
            'quantity' => 18,
            'product_id' => 10,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product_history')->insert([
            'id' => 11,
            'name' => "Stock Inicial",
            'description' => "Este producto inicia con 10 elementos",
            'price' => "70000",
            'quantity' => 10,
            'product_id' => 11,
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
    }
}
