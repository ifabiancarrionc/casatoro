<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product')->insert([
            'id' => 1,
            'name' => "Producto 1",
            'price' => "72000",
            'description' => "Esta es la descrpción del producto 1",
            'image' => "image/product.png",
            'quantity' => 10,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product')->insert([
            'id' => 2,
            'name' => "Producto 2",
            'price' => "80000",
            'description' => "Esta es la descrpción del producto 2",
            'image' => "image/product.png",
            'quantity' => 5,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product')->insert([
            'id' => 3,
            'name' => "Producto 3",
            'price' => "81000",
            'description' => "Esta es la descrpción del producto 3",
            'image' => "image/product.png",
            'quantity' => 12,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product')->insert([
            'id' => 4,
            'name' => "Producto 4",
            'price' => "77000",
            'description' => "Esta es la descrpción del producto 4",
            'image' => "image/product.png",
            'quantity' => 8,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product')->insert([
            'id' => 5,
            'name' => "Producto 5",
            'price' => "42000",
            'description' => "Esta es la descrpción del producto 5",
            'image' => "image/product.png",
            'quantity' => 7,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product')->insert([
            'id' => 6,
            'name' => "Producto 6",
            'price' => "18000",
            'description' => "Esta es la descrpción del producto 6",
            'image' => "image/product.png",
            'quantity' => 0,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product')->insert([
            'id' => 7,
            'name' => "Producto 7",
            'price' => "44000",
            'description' => "Esta es la descrpción del producto 7",
            'image' => "image/product.png",
            'quantity' => 13,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product')->insert([
            'id' => 8,
            'name' => "Producto 8",
            'price' => "35000",
            'description' => "Esta es la descrpción del producto 8",
            'image' => "image/product.png",
            'quantity' => 8,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product')->insert([
            'id' => 9,
            'name' => "Producto 9",
            'price' => "18000",
            'description' => "Esta es la descrpción del producto 9",
            'image' => "image/product.png",
            'quantity' => 0,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product')->insert([
            'id' => 10,
            'name' => "Producto 10",
            'price' => "39000",
            'description' => "Esta es la descrpción del producto 10",
            'image' => "image/product.png",
            'quantity' => 18,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('product')->insert([
            'id' => 11,
            'name' => "Producto 11",
            'price' => "70000",
            'description' => "Esta es la descrpción del producto 11",
            'image' => "image/product.png",
            'quantity' => 10,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
