var tpl = null;
jQuery(document).ready(function ($) {
    $.get('/tpl/product.tpl', function (tpldata) {
        tpl = tpldata;
    });
    var paginate = function () {
        $('.pagination a').on('click', function (e) {
            e.preventDefault();
            if (!$(this).parent('li').hasClass('disabled')) {
                element = $(e);
                $.ajax({
                    url: $(this).attr('href'),
                    data: 'json',
                    method: 'get',
                    success: function (data) {
                        var rendered = Mustache.render(tpl, data);
                        $('.product-list').html(rendered);
                        $('.page-item').removeClass('active');
                        $('.page' + data.current_page).addClass('active');
                        if (data.next_page_url) {
                            $('.page-next').removeClass('disabled').find('page-item').attr('href', data.next_page_url);
                        } else {
                            $('.page-next').addClass('disabled');
                        }
                        if (data.prev_page_url) {
                            $('.page-before').removeClass('disabled').find('page-item').attr('href', data.prev_page_url);
                        } else {
                            $('.page-before').addClass('disabled');
                        }
                    }
                });
            }
        })
    };
    paginate();
});