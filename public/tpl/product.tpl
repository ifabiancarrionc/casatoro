{{#data}}
<tr class="gradeX element">
    <td><a href="edit/{{ id }}">{{ name }}</a></td>
    <td>${{ total }}</td>
    <td>{{ quantity }} x ${{ price }}</td>
</tr>
{{/data}}