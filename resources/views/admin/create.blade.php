@extends('base.base_layout', [
'header_anonymous'  => 1,
'header_auth'       => 0,
'menu_main'         => 0,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Crear Producto')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div class="container">
    <div class="row mb-5 mt-3">
        <div class="col-md-6">
            <h2 class="section-title mb-3">Crear Producto</h2>
        </div>
    </div>
    <!-- /.row -->
    <form role="form" action="/" method="POST" enctype="multipart/form-data">
        <div class="form-row">
            <div class="col-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Nombre</label>
                    <input name="name" value="{{ old('name') }}" class="form-control" placeholder="Nombre">
                    <p class="help-block text-danger">{{ $errors->first('name') }}</p>
                </div>
                <div class="form-group">
                    <label>Descripción</label>
                    <textarea name="description" class="form-control" placeholder="Descripción">{{ old('description') }}</textarea>
                    <p class="help-block text-danger">{{ $errors->first('description') }}</p>
                </div>
                <div class="form-group">
                    <label>Imagen</label>
                    <input name="image" type="file" class="form-control-file">
                    <p class="help-block text-danger">{{ $errors->first('image') }}</p>
                </div>
            </div>
            <div class="col-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Cantidad</label>
                    <input name="quantity" class="form-control" placeholder="Cantidad">
                    <p class="help-block text-danger">{{ $errors->first('quantity') }}</p>
                </div>
                <div class="form-group">
                    <label>Precio</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">$</div>
                        </div>
                        <input name="price" class="form-control" placeholder="Precio">
                    </div>
                    <p class="help-block text-danger">{{ $errors->first('price') }}</p>
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-default">Crear</button>
                <a href="/" class="btn btn-default">Volver al listado</a>
            </div>
        </div>
    </form>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop