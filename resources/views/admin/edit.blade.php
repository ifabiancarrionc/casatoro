@extends('base.base_layout', [
'header_anonymous'  => 1,
'header_auth'       => 0,
'menu_main'         => 0,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Editar Producto')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
@if(Session::has('success'))
<div class='alert alert-success' role='alert'>{{Session::get('success')}}</div>
@endif
<div class="container">
    <div class="row mb-5 mt-3">
        <div class="col-md-6">
            <h2 class="section-title mb-3">Producto: {{ $product->name }}</h2>
        </div>
    </div>
    <!-- /.row -->
    <form role="form" action="/{{ $product->id}}" method="POST" enctype="multipart/form-data">
        <div class="form-row">
            <div class="col-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Nombre</label>
                    <input name="name" value="{{ old('name')?old('name'):$product->name }}" class="form-control" placeholder="Nombre">
                    <p class="help-block text-danger">{{ $errors->first('name') }}</p>
                </div>
                <div class="form-group">
                    <label>Descripción</label>
                    <textarea name="description" class="form-control" placeholder="Descripción">{{ old('description')?old('description'):$product->description }}</textarea>
                    <p class="help-block text-danger">{{ $errors->first('description') }}</p>
                </div>
                <div class="form-group">
                    <figure>
                        <img src="/{{ $product->image }}" height="100">
                    </figure>
                    <label>Imagen</label>
                    <input name="image" type="file" class="form-control-file">
                    <p class="help-block text-danger">{{ $errors->first('image') }}</p>
                </div>
            </div>
            <div class="col-6">
                <!-- /.panel-heading -->
                <div class="form-group">
                    <label>Cantidad</label>
                    <input name="quantity" class="form-control" placeholder="Cantidad" value="{{ old('quantity')?old('quantity'):$product->quantity }}">
                    <p class="help-block text-danger">{{ $errors->first('quantity') }}</p>
                </div>
                <div class="form-group">
                    <label>Precio</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">$</div>
                        </div>
                        <input name="price" class="form-control" placeholder="Precio" value="{{ old('price')?old('price'):$product->price }}">
                    </div>
                    <p class="help-block text-danger">{{ $errors->first('price') }}</p>
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-default">Editar</button>
                <a href="/" class="btn btn-default">Volver al listado</a>
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-md-12">
            <h2 class="section-title mb-3">History</h2>
        </div>
        <div class="col-md-12">
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Valor Total</th>
                        <th>Resumen</th>
                        <th>Fecha/Hora</th>
                        <th>Realizado por</th>
                    </tr>
                </thead>
                <tbody class="product-list">
                    @foreach ($product->productHistory as $log)
                    <tr class="gradeX element">
                        <td>{{ $log->name }}</td>
                        <td>{{ $log->description }}</td>
                        <td>${{ $log->price*$log->quantity }}</td>
                        <td>{{$log->quantity}} x ${{ $log->price }}</td>
                        <td>{{$log->created_at }}</td>
                        <td>{{$log->user->name}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop