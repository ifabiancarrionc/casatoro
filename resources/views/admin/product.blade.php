@extends('base.base_layout', [
'header_anonymous'  => 1,
'header_auth'       => 0,
'menu_main'         => 0,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Productos')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div class="container">
    <div class="row mb-5 mt-3">
        <div class="col-md-6">
            <h2 class="section-title mb-3">Productos</h2>
        </div>
        <div class="col-md-6 text-right">
            <a class="btn btn-primary" href="/new"><i class="icon-plus"></i> Agregar producto</a>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Valor Total</th>
                        <th>Resumen</th>
                    </tr>
                </thead>
                <tbody class="product-list">
                    @foreach ($products as $product)
                    <tr class="gradeX element" id="element{{ $product['data']['id'] }}">
                        <td><a href="/{{ $product->id }}/edit">{{ $product->name }}</a></td>
                        <td>${{ $product->price*$product->quantity }}</td>
                        <td>{{$product->quantity}} x ${{ $product->price }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row mb-5 justify-content-center">
                {{ $products->links('base.paginator') }}
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop