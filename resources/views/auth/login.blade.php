@extends('base.base_layout', [
'header_anonymous'  => 1,
'header_auth'       => 0,
'menu_main'         => 0,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Iniciar Sesión')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div class="container d-flex  justify-content-center mt-3 mb-3">
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">
                <h3><strong>Iniciar Sesión </strong></h3>
            </div>

            <div class="card-body">
                <form action="/login" method="POST">
                    @if($errors->first("password") || $errors->first("email"))
                    <div class="alert alert-danger">
                        <a class="close" data-dismiss="alert" href="#">×</a>
                        @if($errors->first("password"))
                        {!! $errors->first("password") !!}
                        @endif
                        @if($errors->first("email"))
                        {!! $errors->first("email") !!}
                        @endif
                    </div>
                    @endif
                    <div style="margin-bottom: 12px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-username" type="text" class="form-control" name="email" value="" placeholder="Email">                                        
                    </div>

                    <div style="margin-bottom: 12px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="password" placeholder="Contraseña">
                    </div>

                    <div class="input-group">
                        <div class="col checkbox" style="margin-top: 0px;">
                            <label>
                                <input id="login-remember" type="checkbox" name="remember" value="1"> Recuerdame
                            </label>
                        </div>
                        <div class="col">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-center">
                    <a href="/forgot-password">¿Olvidó su contraseña?</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop