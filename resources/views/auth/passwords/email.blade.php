@extends('base.base_layout', [
'header_anonymous'  => 1,
'header_auth'       => 0,
'menu_main'         => 0,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Login')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div class="container d-flex  justify-content-center" style="margin-top:30px">
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">
                <h3><strong>Recordar Contraseña </strong></h3>
            </div>

            <div class="card-body">
                <form role="form" action="/forgot-password" method="POST">
                    @if($errors->first("email"))
                    <div class="alert alert-danger">
                        <a class="close" data-dismiss="alert" href="#">×</a>
                        @if($errors->first("email"))
                        {!! $errors->first("email") !!}
                        @endif
                    </div>
                    @endif
                    @if(Session::get("status"))
                    <div class="alert alert-success">
                        <a class="close" data-dismiss="alert" href="#">×</a>
                        {!! Session::get("status") !!}
                    </div>
                    @endif
                    <div style="margin-bottom: 12px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-username" type="text" class="form-control" name="email" value="" placeholder="Email">                                        
                    </div>
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary">Recuperar Contraseña</button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop