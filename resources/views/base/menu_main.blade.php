@if(isset($menu_main) && $menu_main)

<nav class="site-navigation position-relative text-right" role="navigation">

    <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
        <li><a href="/cart" class="nav-link"><i class="icon-shopping-cart"></i></a></li>
    </ul>
</nav>
@endif