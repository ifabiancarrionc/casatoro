<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::get('/password/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('/forgot-password', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail');
});
Route::group(['middleware' => 'auth'],function(){
    Route::get('/', 'Site\AdminController@index');
    Route::get('api/products', 'Site\AdminController@products');
    Route::get('/new', function(){
        return view('admin.create');
    });
    Route::post('/', 'Site\AdminController@storeProduct');
    Route::get('/{id}/edit', 'Site\AdminController@editProduct');
    Route::post('/{id}', 'Site\AdminController@updateProduct');
});
